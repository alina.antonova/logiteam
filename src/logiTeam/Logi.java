package logiTeam;

import logiTeam.data.EmployeeGrade;
import logiTeam.data.TeamType;
import logiTeam.services.MemberService;
import logiTeam.services.TeamService;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

public class Logi {

    public static void main(String[] args) {

        System.out.println("Hello Logi!");

        TeamService obj1 = new TeamService();

        MemberService obj2 = new MemberService();

        //System.out.println(obj1.createLogiTeam());

        //System.out.println(obj1.printTeam(TeamType.DRACO));

        System.out.println(obj2.updateMemberPhone(new ArrayList<>(), "+4567698", "+23434546743"));

        //System.out.println(obj2.promoteMemberGrade(new ArrayList<>(), "Bob", "Marley", EmployeeGrade.SENIOR));

    }
}