package logiTeam.data;

public enum EmployeeGrade {

    JUNIOR("Junior"),
    MIDDLE("Middle"),
    SENIOR("Senior");

    private String title;

    EmployeeGrade(String title) {
        this.title = title;
    }

    public String title() {
        return title;
    }
}
