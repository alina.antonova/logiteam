package logiTeam.data;

import java.util.List;

public class Team {

    private List<Member> membersList;
    private TeamType teamType;

    public List<Member> getMembersList() {
        return membersList;
    }

    public void setMembersList(List<Member> membersList) {
        this.membersList = membersList;
    }

    public TeamType getType() {
        return teamType;
    }

    public void setType(TeamType teamType) {

        this.teamType = teamType;
    }

    @Override
    public String toString() {
        return "Members: " + membersList + ", Team type: " + teamType;
    }

}

