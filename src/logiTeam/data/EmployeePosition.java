package logiTeam.data;

public enum EmployeePosition {

    JAVADEVELOPER("Java Developer"),
    JSDEVELOPER("Js Developer"),
    MANUALQA("Manual QA"),
    AUTOMATIONQA("Automation QA");

    private String title;

    EmployeePosition(String title) {
        this.title = title;
    }

    public String title() {
        return title;
    }
}
