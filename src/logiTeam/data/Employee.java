package logiTeam.data;

import java.util.Date;

public class Employee extends Person {

    private EmployeePosition position;
    private EmployeeGrade grade;
    private Date startDateInIt;
    private Date startDateInAe;
    private double rate;

    public Employee(String firstName, String lastName, String sex, Date dateOfBirth, EmployeePosition position, EmployeeGrade grade, double rate) {
        super(firstName, lastName, sex, dateOfBirth);
        this.position = position;
        this.grade = grade;
        this.rate = rate;
    }

    public EmployeePosition getPosition() {
        return position;
    }

    public void setPosition(EmployeePosition personPosition) {
        this.position = personPosition;
    }

    public EmployeeGrade getGrade() {
        return grade;
    }

    public void setGrade(EmployeeGrade grade) {
        this.grade = grade;
    }

    public Date getStartDateInIt() {
        return startDateInIt;
    }

    public void setStartDateInIt(Date startDateInIt) {
        this.startDateInIt = startDateInIt;
    }

    public Date getStartDateInAe() {
        return startDateInAe;
    }

    public void setStartDateInAe(Date startDateInAe) {
        this.startDateInAe = startDateInAe;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "Position:: " + position + ", Grade: " + grade + ", Start date in IT: " + startDateInIt
                + ", Start date in AgileEngine: " + startDateInAe + ", Rate: " + rate;
    }

}