package logiTeam.data;

import java.util.Date;

public class Member extends Employee {

    private MemberRole role;
    private boolean isTeamLead;
    private boolean isDeliveryManager;

    public Member(String firstName, String lastName, String sex, Date dateOfBirth, EmployeePosition position, EmployeeGrade grade, double rate, MemberRole role, boolean isTeamLead) {
        super(firstName, lastName, sex, dateOfBirth, position, grade, rate);
        this.role = role;
        this.isTeamLead = isTeamLead;
    }

    public boolean isTeamLead() {
        return isTeamLead;
    }

    public boolean isDeliveryManager() {
        return isDeliveryManager;
    }

    public void setTeamLead(boolean teamLead) {
        isTeamLead = teamLead;
    }

    public void setDeliveryManager(boolean deliveryManager) {
        isDeliveryManager = deliveryManager;
    }

    public MemberRole getRole() {
        return role;
    }

    public void setRole(MemberRole role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Member name: " + getFirstName() + " " + getLastName() + ", Member role: " + role + ", Team Lead: " + isTeamLead + ", Delivery Manager: " + isDeliveryManager;
    }

}
