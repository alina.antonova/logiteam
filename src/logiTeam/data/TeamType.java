package logiTeam.data;

public enum TeamType {

    DRACO("Draco"),
    ARIES("Aries"),
    COLUMBA("Columba"),
    QA("QA");

    private String title;

    TeamType(String title) {
        this.title = title;
    }

    public String title() {
        return title;
    }
}
