package logiTeam.data;

public enum MemberRole {

    DEVELOPER("Developer"),
    QA("QA"),
    PRODUCTOWNER("ProductOwner"),
    DEVOPS("DevOps"),
    HR("Hr");

    private String title;

    MemberRole(String title) {
        this.title = title;
    }

    public String title() {
        return title;
    }
}
