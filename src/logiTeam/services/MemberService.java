package logiTeam.services;

import logiTeam.data.*;
import logiTeam.validators.memberValidator;

import java.util.ArrayList;
import java.util.List;

public class MemberService {

    public List<Member> updateMemberPhone(List<Member> memberList, String oldPhoneNumber, String newPhoneNumber) {

        memberValidator validator = new memberValidator();
        validator.catchEmptyList(memberList);
        validator.catchNullPointerException(oldPhoneNumber);

        for (Member member : memberList) {
            if (member.getPhoneNumber().equals(oldPhoneNumber)) {
                member.setPhoneNumber(newPhoneNumber);
            }
            memberList.add(member);
        };
        return memberList;
        //remove additinal object creation
        //add {} in if methods
        //add validators package + memberValidator class and implement methods for exception catch finally ot thought (via if conditions)
        //oldPhoneNumber = null
        //memberList = empty - return user-friendly error
        //checked/unchecked errors
        //spring investigation
        //REST Azure
    }

    public List<Member> promoteMemberGrade(List<Member> list, String firstName, String lastName, EmployeeGrade newGrade) {

        for (Member member:list) {
            if (member.getFirstName().equals(firstName) && member.getLastName().equals(lastName)) {
                member.setGrade(newGrade);
            }
            list.add(member);
        }
        return list;
    }
}
