package logiTeam.services;

import logiTeam.data.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TeamService {

    public List<Team> createLogiTeam() {
        Member member1 = new Member("Bob", "Marley", "Male", new Date(), EmployeePosition.JAVADEVELOPER,
                EmployeeGrade.JUNIOR, 50.32, MemberRole.DEVELOPER, false);

        Member member2 = new Member("Michael", "Jackson", "Male", new Date(), EmployeePosition.JAVADEVELOPER,
                EmployeeGrade.MIDDLE, 30.675, MemberRole.DEVELOPER, false);

        Member member3 = new Member("Monika", "Beluchi", "Female", new Date(), EmployeePosition.JAVADEVELOPER,
                EmployeeGrade.SENIOR, 101.1, MemberRole.DEVELOPER, true);

        List<Member> membersList1 = new ArrayList<>();
        membersList1.add(member1);
        membersList1.add(member2);
        membersList1.add(member3);

        Member member4 = new Member("Sarah", "Conor", "Female", new Date(), EmployeePosition.JSDEVELOPER,
                EmployeeGrade.MIDDLE, 70.15, MemberRole.DEVELOPER, false);

        Member member5 = new Member("Teddy", "Willyam", "Male", new Date(), EmployeePosition.JSDEVELOPER,
                EmployeeGrade.MIDDLE, 45, MemberRole.DEVELOPER, false);

        Member member6 = new Member("Jonny", "Phillips", "Male", new Date(), EmployeePosition.JSDEVELOPER,
                EmployeeGrade.SENIOR, 142.1, MemberRole.DEVELOPER, true);

        List<Member> membersList2 = new ArrayList<>();
        membersList1.add(member4);
        membersList1.add(member5);
        membersList1.add(member6);

        Member member7 = new Member("Dan", "Nolan", "Male", new Date(), EmployeePosition.JSDEVELOPER,
                EmployeeGrade.JUNIOR, 43.15, MemberRole.DEVELOPER, false);

        Member member8 = new Member("Garry", "Chin", "Male", new Date(), EmployeePosition.JSDEVELOPER,
                EmployeeGrade.SENIOR, 45, MemberRole.DEVELOPER, true);

        List<Member> membersList3 = new ArrayList<>();
        membersList1.add(member7);
        membersList1.add(member8);

        Member member9 = new Member("Jonny", "Big", "Male", new Date(), EmployeePosition.MANUALQA,
                EmployeeGrade.SENIOR, 123, MemberRole.QA, true);

        Member member10 = new Member("Stella", "Felman", "Female", new Date(), EmployeePosition.AUTOMATIONQA,
                EmployeeGrade.MIDDLE, 56.64, MemberRole.QA, false);

        List<Member> membersList4 = new ArrayList<>();
        membersList1.add(member9);
        membersList1.add(member10);

        Team team1 = new Team();
        team1.setType(TeamType.DRACO);
        team1.setMembersList(membersList1);

        Team team2 = new Team();
        team2.setType(TeamType.COLUMBA);
        team2.setMembersList(membersList2);

        Team team3 = new Team();
        team3.setType(TeamType.ARIES);
        team3.setMembersList(membersList3);

        Team team4 = new Team();
        team4.setType(TeamType.QA);
        team4.setMembersList(membersList4);

        List<Team> teams = new ArrayList<>();
        teams.add(team1);
        teams.add(team2);
        teams.add(team3);
        teams.add(team4);

        return teams;
    }

    public Team printTeam(TeamType teamType)
    {
        List<Team> teams = createLogiTeam();

        Team team = new Team();

        for (Team oneTeam:teams) {
            if (oneTeam.getType().equals(teamType)) {
                team.setMembersList(oneTeam.getMembersList());
                team.setType(teamType);
            }
        }
        return team;
    }
}
